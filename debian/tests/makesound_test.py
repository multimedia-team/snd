#!/usr/bin/python3

import sys
import soundfile

frames = 44100
channel = [0] * frames
channel[10] = 0.5

with soundfile.SoundFile(sys.argv[1]) as f:
    if f.frames != frames:
        raise Exception("expected %d frames but got %d!" % (frames, f.frames))
    if not all(channel == f.read()):
        raise Exception("soundfile data does not match")
    print("success!")
